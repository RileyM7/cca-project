const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
//app.use(express.static('static')) //we do not need static files for microservices
app.use (express.urlencoded({extended: false}))
const cors = require('cors') // new for microservice
app.use(cors()) // new for microservice
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port:${port}`))

app.get('/', (req, res) => { // default page with no year input in URL
    res.send('Microservice 1 Gateway by Riley Miranda and Timothy Nicholson. Usage: host/DaysWeeksUntil?year=xxxx');
})
app.get('/DaysWeeksUntil', function (req, res) {
    var result // String to get sent as response to AJAX GET Request
    let futureYearString = req.query.year; // gets year string from URL
    let futureYear = parseInt(req.query.year);
    today = new Date();
    if (isNaN(futureYear)) {result = "Error: '" + futureYearString + "' is not a valid year.";}
    else if (futureYear <= today.getFullYear()) {result = "Error: Can't calculate days and weeks until that year.";}
    else {
        var futureYearDate = new Date(futureYear, 0, 01); // first day of year
        var one_day = 1000*60*60*24; // milliseconds in one day
        let daysUntil = Math.ceil((futureYearDate.getTime() - today.getTime()) / (one_day)) + 1;
        let weeksUntil = Math.ceil(daysUntil / 7);
        result = "There are " + daysUntil + " days and " + weeksUntil + " weeks until the year " + futureYear;
    }
    res.send(result) // gets sent to callback function of jQuery AJAX GET Request in index.html
});